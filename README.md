## qssi-user 11 RKQ1.200928.002 1631956620804 release-keys
- Manufacturer: realme
- Platform: msmnile
- Codename: RMX1931L1
- Brand: realme
- Flavor: qssi-user
- Release Version: 11
- Id: RKQ1.200928.002
- Incremental: 1631956620804
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: qti/msmnile/msmnile:11/RKQ1.200928.002/1631956620804:user/release-keys
- OTA version: 
- Branch: qssi-user-11-RKQ1.200928.002-1631956620804-release-keys
- Repo: realme_rmx1931l1_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
