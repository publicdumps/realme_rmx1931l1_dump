#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/by-name/recovery:83886080:a35173643d8dfd2854a4f6c44fc58461dd256214; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/by-name/boot:100663296:95f02d45e220c2004517b7b802e4b6292e5b4697 \
          --target EMMC:/dev/block/by-name/recovery:83886080:a35173643d8dfd2854a4f6c44fc58461dd256214 && \
      log -t recovery "Installing new oppo recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oppo recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
